/*General
1) Design your framework to support two browsers – chrome and firefox. Make the browser
selection available from a configuration file.
1-Admitir dos navegadores: Chrome y Firefox y hacer que la selección 
del navegador esté disponible desde un archivo de configuración.
Esto se refleja en el archivo ¨package.json¨ 

2) Store the base URL (http://www.musala.com/) in the configuration file
2-Guarde la URL base (http://www.musala.com/) en el archivo de configuración 

3) Make it possible for the three tests below to be executed in parallel simultaneously
3-Hacer posible que las tres pruebas a continuación se ejecuten en paralelo 
simultáneamente 

4) For Test Case 1 – prepare a test data file in a format of your choice, the file must 
contain 5 sets of invalid e-mail addresses. Implement the test to run 5 times with each 
e-mail address. 
4-Para el caso de prueba 1: prepare un archivo de datos de prueba en un formato de 
su elección, el archivo debe contener 5 conjuntos de direcciones de correo electrónico 
no válidas. Implemente la prueba para que se ejecute 5 veces con cada dirección de 
correo electrónico.

5) Provide some kind of report for the test runs 
5 - Proporcione algún tipo de informe para las ejecuciones de prueba

6) If you can think of other useful features – feel free to include them as well!
6-Si puede pensar en otras características útiles, ¡siéntase libre de incluirlas también! 
*/

describe('Test for Musala Soft', () => {
  beforeEach(function () {
    cy.visit('http://www.musala.com/')
  })

  /*Test Case 1
1) Visit http://www.musala.com/
1-Visitar http://www.musala.com/

2) Scroll down and go to ‘Contact Us’
2-Desplácese hacia abajo y vaya a 'Contáctenos'

3) Fill all required fields except email
3-Complete todos los campos obligatorios excepto el correo electrónico

4) Under email field enter string with wrong email format (e.g. test@test)
4-En el campo de correo electrónico, ingrese la cadena con el formato de 
correo electrónico incorrecto (por ejemplo, prueba@prueba)

5) Click ‘Send’ button
5-Haga clic en el botón 'Enviar'

6) Verify that error message ‘The e-mail address entered is invalid.’ appears
6-Verifique que aparezca el mensaje de error "La dirección de correo electrónico 
ingresada no es válida"
*/

  it('Test Case 1', () => {})

  /*  Test Case 2
1) Visit http://www.musala.com/
1-Visita http://www.musala.com/

2) Click ‘Company’ tap from the top
2-Haga clic en el toque 'Empresa' desde la parte superior

3) Verify that the correct URL (http://www.musala.com/company/) loads
3-Verifique que se cargue la URL correcta (http://www.musala.com/company/)

4) Verify that there is ‘Leadership’ section
4-Verifique que haya una sección de 'Liderazgo'

5) Click the Facebook link from the footer
5-Haga clic en el enlace de Facebook desde el pie de página

6) Verify that the correct URL (https://www.facebook.com/MusalaSoft?fref=ts) loads 
and verify if the Musala Soft profile picture appears on the new page
6-Verifique que se cargue la URL correcta (https://www.facebook.com/MusalaSoft?fref=ts) y verifique
si la imagen de perfil de Musala Soft aparece en la nueva página
*/

  it('Test Case 2', () => {
    /* cy.get('#menu-main-nav-1 > .menu-item-887 > .main-link').click()
    cy.get('.cm-content > h2').contains('Leadership')
    cy.get('#wt-cli-accept-all-btn').click()
    cy.get('[href="https://www.facebook.com/MusalaSoft?fref=ts"] > .musala')
      .should('be.visible')
      .click()
    cy.url().should('eq', 'https://www.facebook.com/MusalaSoft?fref=ts')*/
  })

  /*
Test Case 3
1) Visit http://www.musala.com/
1-Visita http://www.musala.com/

2) Navigate to Careers menu (from the top)
2-Vaya al menú Carreras (desde la parte superior)

3) Click ‘Check our open positions’ button
3-Haga clic en el botón "Consultar nuestras posiciones abiertas"

4) Verify that ‘Join Us’ page is opened (can verify that URL is correct:
http://www.musala.com/careers/join-us/
4-Verifique que la página "Únase a nosotros" esté abierta (puede verificar que la URL sea correcta:
http://www.musala.com/careers/join-us/

5) From the dropdown ‘Select location’ select ‘Anywhere’
5-Desde el menú desplegable 'Seleccionar ubicación' seleccione 'Cualquier lugar'

6) Choose open position by name (e.g. Experienced Automation QA Engineer)
6-Elija el puesto vacante por nombre (por ejemplo, ingeniero de control 
de calidad de automatización con experiencia)

7) Verify that 4 main sections are shown: General Description, Requirements, 
Responsibilities, What we offer
7-Verifique que se muestren 4 secciones principales: Descripción General, Requisitos, 
Responsabilidades,Lo que ofrecemos

8) Verify that ‘Apply’ button is present at the bottom
Verifique que el botón 'Aplicar' esté presente en la parte inferior
8-Verifique que el botón 'Aplicar' esté presente en la parte inferior

9) Click ‘Apply’ button
9-Haga clic en el botón 'Aplicar'

10) Prepare a few sets of negative data (e.g. leave required field(s) empty, enter e-mail with
invalid format etc)
10-Prepare algunos conjuntos de datos negativos (p. ej., deje vacíos los campos requeridos, ingrese el correo electrónico con
formato inválido, etc.)

11) Upload a CV document, and click ‘Send’ button
11-Cargue un documento CV y haga clic en el botón 'Enviar'

12) Verify shown error messages (e.g. The field is required, The e-mail address entered is invalid
etc.)
12-Verifique los mensajes de error que se muestran (por ejemplo, el campo es obligatorio, la dirección de correo electrónico ingresada no es válida)
etc.)
*/

  it('Test Case 3', () => {})

  /*Test Case 4
1) Visit http://www.musala.com/
1-Visita http://www.musala.com/

2) Go to Careers
2-Ir a Carreras

3) Click ‘Check our open positions’ button
3-Haga clic en el botón "Consultar nuestras posiciones abiertas"

4) Filter the available positions by available cities in the dropdown ‘Select location’ 
(Sofia and Skopje)
4-Filtre las posiciones disponibles por ciudades disponibles en el menú desplegable 'Seleccionar ubicación' (Sofía y
Skopie)

5) Get the open positions by city
5-Obtenga las posiciones abiertas por ciudad

6) Print in the console the list with available positions by city in the following format:
Example:
Sofia
Position: Database Administrator
More info: http://www.musala.com/job/database-administrator/
Position: OS and Application Administrator
More info: http://www.musala.com/job/os-and-application-administrator/
Position: ………………
More info: ………………
………………………………
Skopje
Position: ………………
More info: ………………
6-Imprime en la consola la lista con los puestos disponibles por ciudad en el siguiente formato:
Ejemplo:
Sofía
Cargo: Administrador de base de datos
Más información: http://www.musala.com/job/database-administrator/
Puesto: Administrador de SO y aplicaciones
Más información: http://www.musala.com/job/os-and-application-administrator/
Posición: ………………
Más información: ………………
………………………………
Skopje
Posición: ………………
Más información: ………………
  */

  it('Test Case 4', () => {})
})
